package job.gaishnik.controller;

import job.gaishnik.controllers.CarNumberController;
import job.gaishnik.dto.CarNumberDto;
import job.gaishnik.services.CarNumberGenerator;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(CarNumberController.class)
@DisplayName("RegistrationController is working when ...")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@TestPropertySource(properties =
        {"spring.autoconfigure.exclude = org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration, " +
                " org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration"})
@ExtendWith(SpringExtension.class)
class CarNumberGeneratorTest {

    private static final CarNumberDto CARNUMBER1 = CarNumberDto.builder().number("А000АА").build();
    private static final CarNumberDto CREATED_CARNUMBER1 = CarNumberDto.builder().id(1L).number("А000АА").build();
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CarNumberGenerator carNumberGenerator;
    @BeforeEach
    void setUp() throws IOException {
        when(carNumberGenerator.random()).thenReturn(CREATED_CARNUMBER1);
    }

    @Test
    public void return_200_random() throws Exception {
        mockMvc.perform(get("/number/random"))
                .andExpect(status().is2xxSuccessful());
    }
    @Test
    public void return_200_next() throws Exception {
        mockMvc.perform(get("/number/next"))
                .andExpect(status().is2xxSuccessful());
    }


}

