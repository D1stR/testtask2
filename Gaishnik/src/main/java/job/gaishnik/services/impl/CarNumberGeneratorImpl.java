package job.gaishnik.services.impl;

import job.gaishnik.dto.CarNumberDto;
import job.gaishnik.exceptions.CarNumberAlreadyTakenException;
import job.gaishnik.exceptions.NextNumberException;
import job.gaishnik.exceptions.RandomNumberException;
import job.gaishnik.models.CarNumber;
import job.gaishnik.repositories.CarNumberRepository;
import job.gaishnik.services.CarNumberGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Random;

import static job.gaishnik.dto.CarNumberDto.from;

@RequiredArgsConstructor
@Service
public class CarNumberGeneratorImpl implements CarNumberGenerator {


    private final CarNumberRepository carNumberRepository;

    @Override
    public CarNumberDto random() throws NextNumberException {
        String number = generateNumber();
        boolean exists = carNumberRepository.existsByNumber(number);
        if (exists) {
            throw new CarNumberAlreadyTakenException("Car number is already taken");
        }
        CarNumber newCarNumber = CarNumber.builder()
                .number(number + " 116 RUS")
                .build();
        return from(carNumberRepository.save(newCarNumber));
    }

    private String lastNumber;


    @Override
    public CarNumberDto next() throws RandomNumberException {
        String lastNumber ;
        if(carNumberRepository.findAll().isEmpty()){
            lastNumber = "Х999ХХ 116 RUS";
        }else{
            lastNumber = carNumberRepository.findFirstByOrderByIdDesc().getNumber();
        }
        String number = generateNextNumber(lastNumber);
        boolean exists = carNumberRepository.existsByNumber(number);
        if (exists) {
            throw new CarNumberAlreadyTakenException("Car number is already taken");
        }
        CarNumber newCarNumber = CarNumber.builder()
                .number(number + " 116 RUS")
                .build();
        return from(carNumberRepository.save(newCarNumber));

    }

    private String generateNextNumber(String number) {
        String str = "АЕТОРНУКХСВМ";
        char[] chars = str.toCharArray();
        Arrays.sort(chars);
        String letters = new String(chars);
        String digits = "0123456789";
        int letterIndex = letters.indexOf(number.charAt(0));
        int firstDigit = Character.getNumericValue(number.charAt(1));
        int secondDigit = Character.getNumericValue(number.charAt(2));
        int thirdDigit = Character.getNumericValue(number.charAt(3));
        int secondLetterIndex = letters.indexOf(number.charAt(4));
        int thirdLetterIndex = letters.indexOf(number.charAt(5));

        if (thirdDigit == 9) {
            thirdDigit = 0;
            if (secondDigit == 9) {
                secondDigit = 0;
                if (firstDigit == 9) {
                    firstDigit = 0;
                    if (thirdLetterIndex == letters.length() - 1) {
                        thirdLetterIndex = 0;
                        if ((secondLetterIndex == letters.length() - 1)) {
                            secondLetterIndex = 0;
                            if (letterIndex == letters.length() - 1) {
                                letterIndex = 0;
                            } else {
                                letterIndex++;
                            }
                        } else {
                            secondLetterIndex++;
                        }
                    } else {
                        thirdLetterIndex++;
                    }
                } else {
                    firstDigit++;
                }
            } else {
                secondDigit++;
            }
        } else {
            thirdDigit++;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(letters.charAt(letterIndex));
        sb.append(firstDigit);
        sb.append(secondDigit);
        sb.append(thirdDigit);
        sb.append(letters.charAt(secondLetterIndex));
        sb.append(letters.charAt(thirdLetterIndex));
        return sb.toString();
    }

    private String generateNumber() {
        String letters = "АЕТОРНУКХСВМ";
        String digits = "0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        sb.append(letters.charAt(random.nextInt(letters.length())));
        sb.append(digits.charAt(random.nextInt(digits.length())));
        sb.append(digits.charAt(random.nextInt(digits.length())));
        sb.append(digits.charAt(random.nextInt(digits.length())));
        sb.append(letters.charAt(random.nextInt(letters.length())));
        sb.append(letters.charAt(random.nextInt(letters.length())));
        return sb.toString();
    }

}
