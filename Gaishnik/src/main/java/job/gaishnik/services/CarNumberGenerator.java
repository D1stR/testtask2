package job.gaishnik.services;

import job.gaishnik.dto.CarNumberDto;
import job.gaishnik.models.CarNumber;

public interface CarNumberGenerator {
    CarNumberDto random();
    CarNumberDto next();

}
