package job.gaishnik.controllers;

import job.gaishnik.services.CarNumberGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/number")
public class CarNumberController {
    private final CarNumberGenerator carNumberGenerator;


    @GetMapping("/random")
    public ResponseEntity<?> getRandom() {
        return ResponseEntity.ok(carNumberGenerator.random());
    }

    @GetMapping("/next")
    public ResponseEntity<?> getNext() {
        return ResponseEntity.ok(carNumberGenerator.next());
    }
}
