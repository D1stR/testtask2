package job.gaishnik.dto;

import job.gaishnik.models.CarNumber;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarNumberDto {
    private Long id;
    private String number;

    public static CarNumberDto from(CarNumber carNumber) {
        return CarNumberDto.builder()
                .number(carNumber.getNumber())
                .build();
    }

    public static List<CarNumberDto> from(List<CarNumber> carNumbers) {
        return carNumbers.stream().map(CarNumberDto::from).collect(Collectors.toList());
    }
}
