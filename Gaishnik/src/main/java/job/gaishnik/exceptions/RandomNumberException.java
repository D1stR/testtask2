package job.gaishnik.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RandomNumberException extends UnsupportedOperationException{
    public RandomNumberException(){
        super("Random number generation is not supported for sequential generator");
    }
}
