package job.gaishnik.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NextNumberException extends UnsupportedOperationException{
    public NextNumberException(){
        super("Next number generation is not supported for random generator");
    }
}
