package job.gaishnik.exceptions;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@AllArgsConstructor
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CarNumberAlreadyTakenException extends RuntimeException {
    public CarNumberAlreadyTakenException(final String message){
        super(message);
    }
}
