package job.gaishnik.repositories;

import job.gaishnik.models.CarNumber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CarNumberRepository extends JpaRepository<CarNumber,Long> {
    Boolean existsByNumber(String number);
    CarNumber findFirstByOrderByIdDesc();

}
