# Тестовое задание "Гаишник" #

## Функционал 💻👩‍💻🕙📆

- Рандомная генерация автомобильного номера
- Последовательная генерация автомобильного номера
## Как проверить
- Запрос: GET http://localhost:8080/number/random
- Ответ: C399BA 116 RUS
***
- Запрос: GET http://localhost:8080/number/next
- Ответ: C400BA 116 RUS

## Используемый стек технологий 💻📱🧑🏖

- Java 17
- [Spring Boot](https://spring.io/projects/spring-boot/)
- [H2 Database](https://www.h2database.com/html/main.html)
- [Lombok](https://projectlombok.org/)
- [SpringBootTest](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/test/context/SpringBootTest.html)





## Демонстрация проекта 💻📱📍🗺

- [Видео-демонстрация](https://youtu.be/_ixMaPcGXXc)
***
![](https://sun9-20.userapi.com/impg/bWMtOJlNAEnZUf9XM1R_6vo9eSi2QmBdABdSOg/sPlrF0iShB4.jpg?size=1841x928&quality=96&sign=68466fa92fd54f48a989b13dc5b191b5&type=album)
***
[Telegram создателя 💬🐤](https://t.me/D1stRRR)
***
